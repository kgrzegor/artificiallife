#include "tst_healthtests.h"

HealthTests::HealthTests()
{

}

HealthTests::~HealthTests()
{

}

void HealthTests::initTestCase()
{

}

void HealthTests::cleanupTestCase()
{

}

void HealthTests::on_creation_max_health_is_set_correctly()
{
    Health health;
    QCOMPARE(health.getMaxHealth(), 100.0f);
}

QTEST_MAIN(HealthTests)
