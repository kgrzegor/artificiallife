QT += testlib
QT -= gui

CONFIG += qt console warn_on depend_includepath testcase
CONFIG -= app_bundle

TEMPLATE = app

HEADERS +=  \
    tst_healthtests.h \
    ../ArtificialLife/health.h

SOURCES += \
    tst_healthtest.cpp  \
    ../ArtificialLife/health.cpp

