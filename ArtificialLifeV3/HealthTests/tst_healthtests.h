#include <QtTest>
#include <QCoreApplication>

#include "../ArtificialLife/health.h"

// add necessary includes here

class HealthTests : public QObject
{
    Q_OBJECT

public:
    HealthTests();
    virtual ~HealthTests();

private slots:
    void initTestCase();
    void cleanupTestCase();
    void on_creation_max_health_is_set_correctly();

};


