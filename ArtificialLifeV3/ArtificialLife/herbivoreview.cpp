#include "herbivoreview.h"
#include <QDebug>

HerbivoreView::HerbivoreView(Map* map, Vector2 position, int debug) : AnimalView(map, position, debug)
{
    qDebug() << "herbivore before created";
    myType = ObjectType::Herbivore;
    QPixmap pixmap(":/icons/animal.png");
    this->setPixmap(pixmap);
    initializeStates();
    setRandomDirection();
    machine.changeState("search");
   // qDebug() << "Herbivore created";
}

void HerbivoreView::initializeStates() // Every state needs to be created separetly and all expected functions should be bound there
{
    machine.addState("search", createSearchState());
    machine.addState("follow", createFollowState());
    machine.addState("eat", createEatState());
    machine.addState("run", createRunState());
    machine.addState("breed", createBreedState());
    machine.addState("rest", createRestState());
}

State* HerbivoreView::createSearchState()
{
    State* state = new State();
    connect(state, SIGNAL(onUpdate()), this, SLOT(searchingForFood()));

    connect(state, SIGNAL(onEnter()), this, SLOT(enterSearch()));
    return state;
}

State* HerbivoreView::createFollowState()
{
    State* state = new State();
    connect(state, SIGNAL(onUpdate()), this, SLOT(followingFood()));
    return state;
}

State* HerbivoreView::createEatState()
{
    State* state = new State();
    connect(state, SIGNAL(onUpdate()), this, SLOT(eating()));
    return state;
}

State* HerbivoreView::createRunState()
{
    State* state = new State();
    connect(state, SIGNAL(onUpdate()), this, SLOT(runningAway()));
    return state;
}

State* HerbivoreView::createBreedState()
{
    State* state = new State();
    connect(state, SIGNAL(onUpdate()), this, SLOT(breeding()));
    return state;
}

State* HerbivoreView::createRestState()
{
    State* state = new State();
    connect(state, SIGNAL(onUpdate()), this, SLOT(resting()));
    return state;
}

void HerbivoreView::searchingForFood()
{
    go();
    scanSurrounding();
    OnMapObject* danger = findCarnivoreInRange();
    OnMapObject* food = findPlantInRange();
    OnMapObject* partner = findPartner();

    if(danger!=nullptr)
    {
        setTarget(danger);
        machine.changeState("run");
        return;
    }
    if(food!=nullptr)
    {
        setTarget(food);
        machine.changeState("follow");
    }
    if(partner!=nullptr && vm->isBreedingAge())
    {
        setTarget(partner);
        machine.changeState("breed");
    }
}

void HerbivoreView::followingFood()
{
    if(!realPosition.isInRange(target->getPosition(), perception))
    {
        target=nullptr;
        machine.changeState("search");
        return;
    }

    if(realPosition.isInRange(target->getPosition(), attackRange))
    {
        target->eat();
    }

    follow(target);
}

void HerbivoreView::eating()
{
    scanSurrounding();
    OnMapObject* danger = findCarnivoreInRange();

    if(danger!=nullptr)
    {
        setTarget(danger);
        machine.changeState("run");
        return;
    }
}

void HerbivoreView::runningAway()
{
    scanSurrounding();

    if(!realPosition.isInRange(target->getPosition(), perception))
    {
        target=nullptr;
        machine.changeState("search");
        return;
    }

    runAwayFrom(target);
}

void HerbivoreView::breed(AnimalViewModel *animal)
{
    map->instantiateHerbivore(animal,this->getPosition());
}

void HerbivoreView::enterSearch()
{

}

void HerbivoreView::resting()
{
    vm->rest(0.001f);
}

void HerbivoreView::breeding()
{
    if(!realPosition.isInRange(target->getPosition(), perception))
    {
        target=nullptr;
        machine.changeState("search");
        return;
    }

    if(getSex()==Sex::female && realPosition.isInRange(target->getPosition(), attackRange))
    {
        ((AnimalView*)target)->startBreeding(vm);
    }

    follow(target);
}

void HerbivoreView::followingPartner()
{

}
