#include "energy.h"

Energy::Energy(float maxEnergy):maxEnergy_(maxEnergy),currentEnergy_(maxEnergy)
{}

void Energy::fatigue(float usedEnergy)
{
    if (usedEnergy < 0)
        return; //exception there
    if (currentEnergy_-usedEnergy < 0)
        currentEnergy_ = 0;

    currentEnergy_-= usedEnergy;
}

void Energy::rest(float energyAmount)
{
    if (energyAmount < 0)
        return; //exception there

    currentEnergy_ += energyAmount;

    if (currentEnergy_ > maxEnergy_)
        currentEnergy_ = maxEnergy_;
}

float Energy::getEnergy() const
{
    return currentEnergy_;
}

float Energy::getMaxEnergy() const
{
    return maxEnergy_;
}
