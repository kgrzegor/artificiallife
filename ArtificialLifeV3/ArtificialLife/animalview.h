#ifndef ANIMAL_H
#define ANIMAL_H

#include "onmapobject.h"
#include "statemachine.h"

class AnimalView: public OnMapObject
{
    Q_OBJECT
public:
    AnimalView(Map *map, Vector2 position, int debug);
    AnimalView(AnimalViewModel* vm, Map* map, Vector2 position);
    void spawn(AnimalViewModel* vm, Vector2 position);
    ~AnimalView();
    void dealDamage(int damage);
    void startBreeding(AnimalViewModel* vm);
    int debug_;

protected:
    AnimalViewModel* vm;
    void setViewModel(AnimalViewModel* vm);
    void exhaust(float value);
    void mousePressEvent(QMouseEvent* event);
    void scanSurrounding();
    virtual void update();
    void setRandomDirection();
    void setDirection(Vector2 direction);
    void go();
    void follow(OnMapObject* obj);
    void runAwayFrom(OnMapObject* obj);
    virtual Sex getSex();
    virtual bool isBreedCapable();
    Vector2 direction;
    float speed;
    float perception;
    OnMapObject* target;
    float attackRange = 2;
    std::list<OnMapObject*> objectsInRange;
    StateMachine machine;

     void setTarget(OnMapObject* target);
     OnMapObject* findPlantInRange();
     OnMapObject* findCarnivoreInRange();
     OnMapObject* findHerbivoreInRange();
     OnMapObject* findPartner();

private slots:
    virtual void searchingForFood() = 0;
    virtual void followingFood() = 0;
    virtual void eating() = 0;
    virtual void runningAway() = 0;
    virtual void enterSearch() = 0;
    virtual void resting() = 0;
    virtual void breeding() = 0;
    virtual void followingPartner() = 0;
    virtual void breed(AnimalViewModel* animal) = 0;
    virtual void die();
    void rest();
    void wakeup();

signals:
    void stats(float,float,float,float,float,float,float,int,Sex);
};

#endif // ANIMAL_H
