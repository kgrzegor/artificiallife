#include "state.h"
#include "engine.h"

State::State(QObject *parent) : QObject(parent)
{
    isActive = false;
    connect(Engine::getInstance(), SIGNAL(updateSignal()), this, SLOT(onEngineUpdate()));
}

void State::enter()
{
    emit onEnter();
    setActive(true);
}

void State::update()
{
    emit onUpdate();
}

void State::exit()
{
    setActive(false);
    emit onExit();
}

void State::setActive(bool active)
{
    isActive = active;
}

void State::onEngineUpdate()
{
    if(isActive)
        update();
}
