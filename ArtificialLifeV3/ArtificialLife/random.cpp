#include "random.h"

std::mt19937 Random::generator(std::random_device{}());

float Random::getMutationRate()
{
    static std::normal_distribution<double> distribution{0,0.1};
    return static_cast<float>(exp(distribution(generator)));
}

bool Random::randomChoice()
{
    return randomChoice(0.5);
}
bool Random::randomChoice(double probability)
{
    std::bernoulli_distribution distribution(probability);
    return distribution(generator);
}

float Random::getRandomFloat(float from, float to)
{
   std::uniform_real_distribution<float> distribution{from, to};
   return distribution(generator);
}
