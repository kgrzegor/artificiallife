#include "statemachine.h"
#include <QDebug>
StateMachine::StateMachine(QObject *parent) : QObject(parent)
{
    currentState = nullptr;
}

void StateMachine::enterState(State* state)
{
    currentState = state;
    state->enter();
}

void StateMachine::exitState(State* state)
{
    if(state!=nullptr)
        state->exit();
}

StateMachine::~StateMachine()
{
    for(auto i = stateMap.begin(); i != stateMap.end(); ++i)
    {
        delete i->second;
    }
}

void StateMachine::changeState(std::string name) //TODO: rzucanie wyjątku w przypadku nieistniejącego state'a
{
    exitState(currentState);
    enterState(stateMap[name]);
}

void StateMachine::addState(std::string name, State* state) // TODO: duplikowane name?
{
    stateMap[name] = state;
}
