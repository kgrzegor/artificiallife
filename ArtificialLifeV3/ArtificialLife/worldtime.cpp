#include "worldtime.h"


WorldTime::WorldTime(MainWindow* window, QObject *parent):QObject(parent), timer_(),window_(window)
{
    day_ = 0;
    hour_ = 0;
    minutes_ = 0;
    current_time_speed_ = 2;
    updateDate();
    updateTimer();
    connect(window_->getMinusButton(), SIGNAL(released()),this,SLOT(decreaseTimeSpeed()));
    connect(window_->getPlusButton(), SIGNAL(released()),this, SLOT(increaseTimeSpeed()));

    timer_.start(1000); //released every second
    connect(&timer_, SIGNAL(timeout()), this, SLOT(updateDate()));
}

WorldTime::~WorldTime(){}

void WorldTime::updateDate()
{
    minutes_ += 30;
    while(minutes_ >= 60)
    {
        minutes_ -=60;
        ++hour_;
        if (hour_>=24)
        {
            hour_-=24;
            ++day_;
            //notifyAllAnimals();
        }
    }


    window_->setDate(day_, hour_, minutes_);
}

void WorldTime::updateTimer()
{
    timer_.setInterval(static_cast<int>(1000.0f/time_speeds_[current_time_speed_]));
    window_->setTime(time_speeds_[current_time_speed_]);
}

void WorldTime::increaseTimeSpeed()
{
    if(current_time_speed_ == time_speeds_.size() - 1)
        return;
    ++current_time_speed_;
    updateTimer();
}

void WorldTime::decreaseTimeSpeed()
{
    if(current_time_speed_ == 0)
        return;
    --current_time_speed_;
    updateTimer();
}
