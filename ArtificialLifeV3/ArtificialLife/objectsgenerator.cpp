#include "objectsgenerator.h"

ObjectsGenerator::ObjectsGenerator(Map* map)
{
    this->map = map;
}

void ObjectsGenerator::generate(int herbivores, int carnivores, int plants)
{
    for(int i=0; i<herbivores; ++i)
    {
        map->instantiateHerbivore(AnimalViewModel::random(), Vector2::getRandomInRange(0, 1280, 0, 720));
    }
    for(int i=0; i<carnivores; ++i)
    {
        map->instantiateCarnivore(AnimalViewModel::random(), Vector2::getRandomInRange(0, 1280, 0, 720));
    }
    for(int i=0; i<plants; ++i)
    {
        map->instantiatePlant(nullptr, Vector2::getRandomInRange(0, 1280, 0, 720));
    }
}
