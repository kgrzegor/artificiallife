#include "staticattributes.h"

StaticAttributes::StaticAttributes(float speed, float perception, float endurance, float strength, float regeneration)
    :  speed_(speed), perception_(perception),endurance_(endurance),strength_(strength),regeneration_(regeneration)
{
     if (Random::randomChoice())
         sex_ = Sex::female;
     else
         sex_ = Sex::male;
}
