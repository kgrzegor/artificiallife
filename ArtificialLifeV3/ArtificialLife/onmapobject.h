#ifndef ONMAPOBJECT_H
#define ONMAPOBJECT_H

#include <QLabel>
#include <QPropertyAnimation>
#include "engine.h"
#include "vector2.h"
#include "map.h"

class OnMapObject : public QLabel //every object which appears on the map should inherit from this, update method is called every frame so use it for moving for example
{

    Q_OBJECT
public:
    enum ObjectType {Carnivore, Herbivore, Plant};
    explicit OnMapObject(Map *map, Vector2 position = Vector2(0,0));
    OnMapObject(Map* map, Vector2 position, ILivingObject* vm);
    void SetLivnigViewModel(ILivingObject* vm);
    void setPosition(Vector2 position);
    Vector2 getPosition() const;
    void moveObj(Vector2 delta);
    void setAnimation(Vector2 from, Vector2 to, int duration = 10000);
    ObjectType getObjType();
    bool isActive();
    void setActive(bool active);
    void setMap(Map* map);
    void hurt(float damage);
    virtual bool isBreedCapable() = 0;
    float eat();
    virtual void die();
    virtual Sex getSex() = 0;
protected:
    Map* map;
    Vector2 realPosition;
    QPropertyAnimation* animation = 0;
    ObjectType myType;
    ILivingObject* myVM;
    bool active;
    float kalories;
    virtual void update();

private slots:
    void updateSlot();

};

#endif // ONMAPOBJECT_H
