#ifndef ENERGY_H
#define ENERGY_H


class Energy
{
private:
    float maxEnergy_;
    float currentEnergy_;
public:
    Energy(float maxEnergy_);
    void fatigue(float usedEnergy);
    void rest(float energyAmount);
    float getEnergy() const;
    float getMaxEnergy() const;
};

#endif // ENERGY_H
