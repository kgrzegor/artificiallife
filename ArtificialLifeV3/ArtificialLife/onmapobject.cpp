#include "onmapobject.h"
#include <QDebug>

OnMapObject::OnMapObject(Map *map, Vector2 position) : QLabel(map->getWidget())
{
    this->setGeometry(position.x, position.y, 32, 32);
    connect(Engine::getInstance(), SIGNAL(updateSignal()), this, SLOT(updateSlot()));
    this->map = map;
    setPosition(position);
}

OnMapObject::OnMapObject(Map *map, Vector2 position, ILivingObject* vm) : QLabel(map->getWidget())
{
    this->setGeometry(position.x, position.y, 32, 32);
    connect(Engine::getInstance(), SIGNAL(updateSignal()), this, SLOT(updateSlot()));
    this->map = map;
    setPosition(position);

}

void OnMapObject::SetLivnigViewModel(ILivingObject* vm)
{
    myVM = vm;
}

void OnMapObject::update()
{

}

void OnMapObject::hurt(float damage)
{
    if(myVM==nullptr) return;
    myVM->hurt(damage);
    if(myVM->getHealth()<=0)
        die();
}

float OnMapObject::eat()
{
    setActive(false);
    return kalories;
}

void OnMapObject::die()
{

}

void OnMapObject::setPosition(Vector2 position)
{
    //qDebug() << "Setting position to x: " << position.x << " y: " << position.y;
    realPosition = position;
    this->setGeometry(realPosition.x, realPosition.y, geometry().width(), geometry().height());
}

Vector2 OnMapObject::getPosition() const
{
    float x = this->realPosition.x;
    float y = this->realPosition.y;
    Vector2 position(x, y);
    return position;
}

void OnMapObject::moveObj(Vector2 delta)
{
    //this->move(delta.x, delta.y);
    Vector2 destination(0,0);
    destination.x = delta.x + realPosition.x;
    destination.y = delta.y + realPosition.y;
    setPosition(destination);
}

void OnMapObject::setAnimation(Vector2 from, Vector2 to, int duration)
{
    animation = new QPropertyAnimation(this, "geometry");
    animation->setDuration(duration);
    animation->setStartValue(QRect(from.x, from.y, 32, 32));
    animation->setEndValue(QRect(to.x, to.y, 64, 32));
    animation->start();
}

OnMapObject::ObjectType OnMapObject::getObjType()
{
    return myType;
}

void OnMapObject::setActive(bool active)
{
    this->active = active;
    setPosition(Vector2(-100, -100));
}

bool OnMapObject::isActive()
{
    return active;
}

void OnMapObject::setMap(Map* map)
{
    this->map = map;
}

void OnMapObject::updateSlot()
{
    update();
}
