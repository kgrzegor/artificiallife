#include "map.h"
#include "plantview.h"
#include "carnivoreview.h"
#include "herbivoreview.h"
#include <QDebug>
#include "onmapobjectpool.h"

Map::Map(QWidget* widget) //: worldTime(nullptr)
{
    objects.reserve(200);
    this->widget = widget;
    connect(Engine::getInstance(), SIGNAL(updateSignal()), this, SLOT(update()));
    leftUpBorder.y = 0;
    leftUpBorder.x = 0;
    rightDownBorder.y = 720;
    rightDownBorder.x = 1280;
}

void Map::instantiatePlant(Plant* object, Vector2 position) // TODO: bind ILinigObject to AnimalView
{
    PlantView* plant = OnMapObjectPool::getInstnace()->getPlant(this);
    objects.push_back(plant);
    plant->setPosition(position);
}

void Map::instantiateHerbivore(AnimalViewModel* object, Vector2 position)
{
    HerbivoreView* herbivore = OnMapObjectPool::getInstnace()->getHerbivore(this);
    herbivore->spawn(object, position);
   // herbivore->setPosition(position);
    objects.push_back(herbivore);
}

void Map::instantiateCarnivore(AnimalViewModel* object, Vector2 position)
{
    CarnivoreView* carnivore = OnMapObjectPool::getInstnace()->getCarnivore(this);
    carnivore->spawn(object, position);
    //carnivore->setPosition(position);
    objects.push_back(carnivore);
}

QWidget* Map::getWidget()
{
    return widget;
}

std::vector<OnMapObject*>* Map::getMapObjects()
{
    return &objects;
}

void Map::returnObjectsIntoBorders()
{
    for(auto obj : objects)
    {
        Vector2 pos = obj->getPosition();

        if(pos.x < leftUpBorder.x)
            pos.x = rightDownBorder.x;
        else if(pos.x > rightDownBorder.x)
            pos.x = leftUpBorder.x;

        if(pos.y > rightDownBorder.y)
            pos.y = leftUpBorder.y;
        else if(pos.y < leftUpBorder.y)
            pos.y = rightDownBorder.y;

        obj->setPosition(pos);
    }
}

Map::~Map()
{
    int size = objects.size();
    for(int i=0; i<size; ++i)
    {
        delete objects[i];
    }
}

void Map::update()
{
    returnObjectsIntoBorders();
}
