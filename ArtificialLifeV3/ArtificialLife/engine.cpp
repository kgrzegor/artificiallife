#include "engine.h"

Engine* Engine::instance;

Engine* Engine::getInstance()
{
    if(instance==nullptr)
    {
        instance = new Engine();
    }
    return instance;
}

Engine::Engine(QObject *parent) : QObject(parent)
{
    auto timer = new QTimer(parent);
    connect(timer, &QTimer::timeout, []{Engine::getInstance()->update();});
    timer->start();
}

void Engine::update()
{
    emit updateSignal();
}


Engine::~Engine()
{
}
