#ifndef RANDOM_H
#define RANDOM_H
#include <random>
#include <cmath>
class Random
{
private:
    static std::mt19937 generator;
    Random() = delete;
public:
    static float getMutationRate();
    static bool randomChoice();
    static bool randomChoice(double probability);
    static float getRandomFloat(float from, float to);
};

#endif // RANDOM_H
