#include "onmapobjectpool.h"
#include <QDebug>

OnMapObjectPool* OnMapObjectPool::instance;

OnMapObjectPool* OnMapObjectPool::getInstnace()
{
    if(instance==nullptr)
        instance = new OnMapObjectPool();
    return instance;
}

PlantView* OnMapObjectPool::getPlant(Map* map)
{
    for(auto pv : plants)
    {
        if(!pv->isActive())
        {
            pv->setActive(true);
            return pv;
        }
    }
    PlantView* pv = new PlantView(map, Vector2(0,0));
    plants.push_back(pv);
    pv->setActive(true);
    return pv;
}

CarnivoreView* OnMapObjectPool::getCarnivore(Map* map)
{
    for(auto cv : carnivores)
    {
        if(!cv->isActive())
        {
            cv->setActive(true);
            return cv;
        }
    }
    CarnivoreView* cv = new CarnivoreView(map, Vector2(0,0));
    carnivores.push_back(cv);
    cv->setActive(true);
    return cv;
}

HerbivoreView* OnMapObjectPool::getHerbivore(Map* map)
{
    for(auto hv : herbivores)
    {
        if(!hv->isActive())
        {
            hv->setActive(true);
            return hv;
        }
    }
    HerbivoreView* hv = new HerbivoreView(map, Vector2(0,0));
    herbivores.push_back(hv);
    hv->setActive(true);
    return hv;
}

OnMapObjectPool::OnMapObjectPool()
{

}
