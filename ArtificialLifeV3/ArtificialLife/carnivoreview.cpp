#include "carnivoreview.h"
#include <QDebug>

CarnivoreView::CarnivoreView(Map* map, Vector2 position) : AnimalView(map, position, -1)
{
    //qDebug() << "Carnivore before created";
    myType = ObjectType::Carnivore;
    QPixmap pixmap(":/icons/raptor.png");
    this->setPixmap(pixmap);
    initializeStates();
    setRandomDirection();
    machine.changeState("search");
    //qDebug() << "Carnivore created";
}

void CarnivoreView::initializeStates() // Every state needs to be created separetly and all expected functions should be bound there
{
    machine.addState("search", createSearchState());
    machine.addState("follow", createFollowState());
    machine.addState("eat", createEatState());
    machine.addState("followPartner", createFollowPartnerState());
    machine.addState("rest", createRestState());
}

State* CarnivoreView::createSearchState()
{
    State* state = new State();
    connect(state, SIGNAL(onUpdate()), this, SLOT(searchingForFood()));

    connect(state, SIGNAL(onEnter()), this, SLOT(enterSearch()));
    return state;
}

State* CarnivoreView::createFollowState()
{
    State* state = new State();
    connect(state, SIGNAL(onUpdate()), this, SLOT(followingFood()));
    return state;
}

State* CarnivoreView::createEatState()
{
    State* state = new State();
    connect(state, SIGNAL(onUpdate()), this, SLOT(eating()));
    return state;
}

State* CarnivoreView::createFollowPartnerState()
{
    State* state = new State();
    connect(state, SIGNAL(onUpdate()), this, SLOT(followingPartner()));
    return state;
}

State* CarnivoreView::createRestState()
{
    State* state = new State();
    connect(state, SIGNAL(onUpdate()), this, SLOT(resting()));
    return state;
}

void CarnivoreView::searchingForFood()
{
    go();
    scanSurrounding();
    OnMapObject* food = findHerbivoreInRange();
    OnMapObject* partner = findPartner();

    if(food!=nullptr)
    {
        setTarget(food);
        machine.changeState("follow");
    }

    if(partner!=nullptr)
    {
        setTarget(partner);
        machine.changeState("followPartner");
    }
}

void CarnivoreView::followingFood()
{
    if(!realPosition.isInRange(target->getPosition(), perception))
    {
        target=nullptr;
        machine.changeState("search");
        return;
    }

    if(realPosition.isInRange(target->getPosition(), attackRange))
    {
        if(vm!=nullptr)
        //target->hurt(vm->getAttributes().strength_);
        target->eat();
    }

    follow(target);
}

void CarnivoreView::eating()
{
    machine.changeState("search");
}

void CarnivoreView::runningAway()
{
    machine.changeState("search");
}

void CarnivoreView::searchingForPartner()
{
    go();
    scanSurrounding();
    OnMapObject* food = findHerbivoreInRange();
    OnMapObject* partner = findCarnivoreInRange();

    if(food!=nullptr)
    {
        setTarget(food);
        machine.changeState("follow");
    }

    if(partner!=nullptr)
    {
        setTarget(partner);
        machine.changeState("followPartner");
    }
}

void CarnivoreView::followingPartner()
{
    if(!realPosition.isInRange(target->getPosition(), perception))
    {
        target=nullptr;
        machine.changeState("search");
        return;
    }

    if(getSex() == Sex::female && realPosition.isInRange(target->getPosition(), attackRange))
    {
        qDebug() << "Breeding";
        ((AnimalView*)target)->startBreeding(vm);
        machine.changeState("search");
    }

    follow(target);
}

void CarnivoreView::reproducing()
{

}

void CarnivoreView::enterSearch()
{
}

void CarnivoreView::resting()
{
    vm->rest(0.001f);
}

void CarnivoreView::breeding()
{

}

void CarnivoreView::breed(AnimalViewModel* animal)
{
    map->instantiateHerbivore(animal, this->getPosition());
}
