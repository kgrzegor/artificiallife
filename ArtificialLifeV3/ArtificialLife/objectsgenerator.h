#ifndef OBJECTSGENERATOR_H
#define OBJECTSGENERATOR_H
#include "map.h"

class ObjectsGenerator
{
private:
    Map* map;
public:
    ObjectsGenerator(Map* map);
    void generate(int herbivores, int carnivores, int plants);
};

#endif // OBJECTSGENERATOR_H
