#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QPushButton>
#include "animalview.h"
#include "plantview.h"
#include "map.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();
    void setDate(int day, int hour, int minutes) const;
    void setTime(float time_speed) const;
    QPushButton* getPlusButton() const;
    QPushButton* getMinusButton() const;
private:
    Ui::MainWindow *ui;
    AnimalView* animal;
    AnimalView* animal2;
    PlantView* plant;
    Map* map;

public slots:
    void updateMenu(float speed,float perception ,float endurance,float strength,float regeneration ,float energy,float health,int age, Sex sex);

};

#endif // MAINWINDOW_H
