#ifndef MENUWINDOW_H
#define MENUWINDOW_H

#include "mainwindow.h"
#include "worldtime.h"

#include <QMainWindow>

namespace Ui {
class MenuWindow;
}

class MenuWindow : public QMainWindow
{
    Q_OBJECT
private:
    MainWindow* window;
    WorldTime* world_time_;
public:
    explicit MenuWindow(QWidget *parent = nullptr);
    ~MenuWindow();

private slots:
    void on_start_button_clicked();
    void on_exit_button_clicked();

private:
    Ui::MenuWindow *ui;
};

#endif // MENUWINDOW_H
