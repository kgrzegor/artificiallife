#include "animalview.h"
#include <QDebug>
#include <QElapsedTimer>
AnimalView::AnimalView(Map *map, Vector2 position, int debug) : OnMapObject(map, position), direction(0,0)
  //this is only for ui, will be changed
{
    debug_ = debug;
    speed = 0.002f;
    perception = 100;
    vm=nullptr;
    //this->setStyleSheet(QString("background-color: transparent;border: 5px;"));
    kalories = 20;

    connect(this, SIGNAL(stats(float,float,float,float,float,float,float,int,Sex)), \
            map->getWidget(), SLOT(updateMenu(float,float,float,float,float,float,float,int,Sex)));
}

AnimalView::AnimalView(AnimalViewModel* vm, Map* map, Vector2 position) : OnMapObject(map, position, vm), direction(0,0)
{
    setViewModel(vm);
}

void AnimalView::spawn(AnimalViewModel* vm, Vector2 position)
{
    setActive(true);
    setPosition(position);
    setViewModel(vm);
}

void AnimalView::setViewModel(AnimalViewModel* vm)
{
    this->vm = vm;
     if(vm==nullptr) return;
    speed = vm->getAttributes().speed_;
    perception = vm->getAttributes().perception_;
    connect(vm, SIGNAL(die()), this, SLOT(die()));
    connect(vm, SIGNAL(breed(AnimalViewModel*)), this, SLOT(breed(AnimalViewModel*)));
    connect(vm, SIGNAL(exhausted()), this, SLOT(rest()));
    connect(vm, SIGNAL(rested()), this, SLOT(wakeup()));
    SetLivnigViewModel(vm);
    if(getSex() == Sex::female)
        qDebug() << "Female";
    else if(getSex() == Sex::male)
        qDebug() << "Male";
}

void AnimalView::exhaust(float value)
{
    if(vm==nullptr) return;
    vm->fatigue(value);
}

AnimalView::~AnimalView()
{
    if(vm!=nullptr)
   delete vm;
}

void AnimalView::mousePressEvent(QMouseEvent *event)
{
    const StaticAttributes& attributes = vm->getAttributes();

    float speed = attributes.speed_;
    float perception = attributes.perception_;
    float endurance = attributes.endurance_;
    float strength = attributes.strength_;
    float regeneration = attributes.regeneration_;
    Sex sex = attributes.sex_;

    int age = vm->getAge();
    float energy = vm->getEnergy();
    float health = vm->getHealth();

    emit stats(speed,perception,endurance,strength,regeneration,energy,health,age,sex);
}

void AnimalView::scanSurrounding()
{
    //if(active==false) return;
    std::vector<OnMapObject*>* objects = map->getMapObjects();
    objectsInRange.clear();
    for(OnMapObject* obj : *objects)
    {
        if(obj->isActive() && this != obj && this->getPosition().isInRange(obj->getPosition(), perception))
        {
            objectsInRange.push_back(obj);
        }
    }
}

void AnimalView::update()
{
    static float timer = 0;
    timer+=0.1f;
    if(timer>=2000)
    {
        vm->increaseAge();
        timer=0;
    }
}

void AnimalView::setRandomDirection()
{
    setDirection(Vector2::randomInUnitCircle());
    //qDebug() << "direction x: " << direction.x << " y: " << direction.y;
}

void AnimalView::setDirection(Vector2 direction)
{
    direction.normalize();
    this->direction = direction;
}

void AnimalView::go()
{
    if(active==false) return;
    moveObj(direction * speed);
    exhaust(5.5f);
}

void AnimalView::follow(OnMapObject* obj)
{
    setDirection(realPosition.getVectorTo(obj->getPosition()));
    go();
}

void AnimalView::runAwayFrom(OnMapObject* obj)
{
    setDirection(realPosition.getVectorTo(obj->getPosition()).opposite());
    go();
}

void AnimalView::startBreeding(AnimalViewModel* partner)
{
    qDebug() << "Breeding";
    vm->breed(*vm, *partner);
}

Sex AnimalView::getSex()
{
    if(vm==nullptr) return Sex::dontcare;
    return vm->getAttributes().sex_;
}

bool AnimalView::isBreedCapable()
{
    return vm->isBreedingAge();
}

void AnimalView::setTarget(OnMapObject* target)
{
    this->target = target;
}

OnMapObject* AnimalView::findPlantInRange()
{
    for(OnMapObject* obj : objectsInRange)
    {
        if(obj->getObjType() == OnMapObject::Plant)
        {
            return obj;
        }
    }
    return nullptr;
}

OnMapObject* AnimalView::findCarnivoreInRange()
{
    for(OnMapObject* obj : objectsInRange)
    {
        if(obj->getObjType() == OnMapObject::Carnivore)
        {
            return obj;
        }
    }
    return nullptr;
}

OnMapObject* AnimalView::findHerbivoreInRange()
{
    for(OnMapObject* obj : objectsInRange)
    {
        if(obj->getObjType() == OnMapObject::Herbivore)
        {
            return obj;
        }
    }
    return nullptr;
}

OnMapObject* AnimalView::findPartner()
{
    for(OnMapObject* obj : objectsInRange)
    {
        if(obj->getObjType() == getObjType() && obj->getSex() != getSex() && obj->isBreedCapable())
        {
            qDebug() << "Partner found!";
            return obj;
        }
    }
    return nullptr;
}

void AnimalView::die()
{
    setActive(false);
}

void AnimalView::rest()
{
    qDebug() << "Resting!";
    machine.changeState("rest");
}

void AnimalView::wakeup()
{
    qDebug() << "Waking up!";
    machine.changeState("search");
}

