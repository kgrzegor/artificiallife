#ifndef ANIMALVIEWMODEL_H
#define ANIMALVIEWMODEL_H

#include "energy.h"
#include "health.h"
#include "ilivingobject.h"
#include "sex.h"
#include "staticattributes.h"
#include <QObject>
#include "random.h"
class AnimalViewModel : public QObject, public ILivingObject
{
    Q_OBJECT

private:
    constexpr static int minumum_age_ = 10; //age that all animals can achive without chance that they die from old age
    constexpr static float normal_endurance = 10;
    constexpr static float normal_energy = 100;

    Health health_;
    Energy energy_;
    bool hasBred;
    const StaticAttributes attributes_;
    int age_;

    static float createAttribute(float attribute1, float attribute2);
public:

    AnimalViewModel(float energy, float perception, float endurance, float strength, QObject *parent = nullptr);
    AnimalViewModel(const AnimalViewModel& other);
    AnimalViewModel(AnimalViewModel&&) = default;
    virtual ~AnimalViewModel() = default;

     static AnimalViewModel* breed(AnimalViewModel& parent1, AnimalViewModel& parent2);
    int getAge() const;
     static AnimalViewModel* random();
     void notifyView(AnimalViewModel* newAnimal);
    // ILivingObject interface
    void hurt(float damage);
    void heal(float healthAmount);
    float getHealth() const;
    float getMaxHealth() const;
    bool isBreedingAge() const;
    // Energy interface
    void fatigue(float usedEnergy);
    void rest(float energyAmount);
    float getEnergy() const;
    float getMaxEnergy() const;

    const StaticAttributes& getAttributes() const; //don't know what is better, copying or returning const&


signals:
    void die();
    void breed(AnimalViewModel* animal);
    void exhausted();
    void rested();
public slots:
    void increaseAge();

};

#endif // ANIMALVIEWMODEL_H
