#include "plant.h"

Plant::Plant(float maxHealth) : health(maxHealth)
{}

void Plant::hurt(float damage)
{
    health.hurt(damage);
}
void Plant::heal(float healthAmount)
{
    health.heal(healthAmount);
}
float Plant::getHealth() const
{
    return health.getHealth();
}
