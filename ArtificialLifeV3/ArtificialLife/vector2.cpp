#include "vector2.h"
#include <cmath>
#include <qglobal.h>
#include <QDebug>
#include <worldtime.h>

Vector2::Vector2(float x, float y)
{
    this->x = x;
    this->y = y;
}

float Vector2::getDistanceFrom(Vector2 position)
{
    float xDistance = position.x - x;
    float yDistance = position.y - y;
    return sqrtf(pow(xDistance, 2) + pow(yDistance, 2));
}

bool Vector2::isInRange(Vector2 position, float range)
{
    float xDistanceSquare = (position.x - x) * (position.x - x);
    float yDistanceSquare = (position.y - y) * (position.y - y);
    return range*range >= xDistanceSquare + yDistanceSquare;
}

Vector2 Vector2::getVectorTo(Vector2 target)
{
    Vector2 result;
    result.x = target.x - x;
    result.y = target.y - y;
    return result;
}

float Vector2::getLength()
{
    return sqrtf(x*x + y*y);
}

Vector2 Vector2::getRandomInRange(float minX, float maxX, float minY, float maxY)
{
    Vector2 vec;
    vec.x = Random::getRandomFloat(minX, maxX);
    vec.y = Random::getRandomFloat(minY, maxY);
    //qDebug() << "Generated on x: " << vec.x << " y: " << vec.y;
    return vec;
}

Vector2 Vector2::randomInUnitCircle()
{
    Vector2 vec;
    vec.x = Random::getRandomFloat(-1.0f,1.0f);
    float multiplier = 1.0f;
    if (Random::randomChoice())
        multiplier = -multiplier;

    vec.y = (1 - vec.x * vec.x) * multiplier;
    return vec;
}

Vector2 Vector2::operator*(float multiplier)
{
    return Vector2(x * multiplier, y * multiplier);
}

void Vector2::normalize()
{
    float length = getLength();
    x/=length;
    y/=length;
}


Vector2 Vector2::opposite()
{
    return (*this)*-1;
}
