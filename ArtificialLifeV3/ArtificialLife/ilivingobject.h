#ifndef ILIVINGOBJECT_H
#define ILIVINGOBJECT_H

#include <QObject>

class ILivingObject
{
public:
    virtual ~ILivingObject() {}
    virtual void hurt(float damage) = 0;
    virtual void heal(float healthAmount) = 0;
    virtual float getHealth() const = 0;
};

#endif // ILIVINGOBJECT_H
