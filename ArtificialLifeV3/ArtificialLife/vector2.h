#ifndef VECTOR2_H
#define VECTOR2_H


class Vector2
{
public:
    float x;
    float y;
    Vector2(float x = 0, float y = 0);
    float getDistanceFrom(Vector2 position);
    bool isInRange(Vector2 position, float range);
    Vector2 getVectorTo(Vector2 target);
    float getLength();
    static Vector2 getRandomInRange(float minX, float maxX, float minY, float maxY);
    static Vector2 randomInUnitCircle();
    inline Vector2 operator*(float multiplier);
    void normalize();
    Vector2 opposite();
};

#endif // VECTOR2_H
