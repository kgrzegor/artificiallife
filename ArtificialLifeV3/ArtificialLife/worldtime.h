#ifndef WORLD_TIME_H
#define WORLD_TIME_H

#include <QTimer>
#include <vector>
#include "animalviewmodel.h"
#include "mainwindow.h"
#include <array>

class WorldTime : QObject
{
    Q_OBJECT
private:
    int day_;
    int hour_;
    int minutes_;
    unsigned long long current_time_speed_;
    const std::array<float, 9> time_speeds_ = {{0.2f, 0.5f, 1.0f, 2.0f, 3.0f, 5.0f,10.0f,25.0f,100.0f}};
    QTimer timer_;
    std::vector<AnimalViewModel> animals_;
    MainWindow* window_;

    void updateTimer();
public:
    WorldTime(MainWindow* window, QObject *parent = nullptr);
    virtual ~WorldTime();
private slots:
    void updateDate();
    void increaseTimeSpeed();
    void decreaseTimeSpeed();
signals:
    void notifyAllAnimals();

};

#endif // WORLD_TIME_H
