#ifndef ENGINE_H
#define ENGINE_H

#include <QObject>
#include <QTimer>

class Engine : public QObject
{
   Q_OBJECT
public:

    static Engine* getInstance();

     void update();
    ~Engine();
private:
    explicit Engine(QObject *parent = nullptr);
    static Engine* instance;

signals:
    void updateSignal();

public slots:
};
#endif // ENGINE_H
