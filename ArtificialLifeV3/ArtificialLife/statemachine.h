#ifndef STATEMACHINE_H
#define STATEMACHINE_H

#include <QObject>
#include <map>
#include "state.h"

class StateMachine : public QObject
{
    Q_OBJECT
protected:
    std::map<std::string, State*> stateMap;
    State* currentState;
    void enterState(State* state);
    void exitState(State* state);

public:
    ~StateMachine();
    explicit StateMachine(QObject *parent = nullptr);
    void changeState(std::string name);
    void addState(std::string name, State* state);
};

#endif // STATEMACHINE_H
