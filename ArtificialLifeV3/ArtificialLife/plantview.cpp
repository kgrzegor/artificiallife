#include "plantview.h"
#include <QDebug>

PlantView::PlantView(Map* map, Vector2 position) : OnMapObject (map, position)
{
    myType = ObjectType::Plant;
    QPixmap pixmap(":/icons/apple.png");
    this->setPixmap(pixmap);
    kalories = 10;
}

void PlantView::update()
{

}

Sex PlantView::getSex()
{
    return Sex::dontcare;
}

bool PlantView::isBreedCapable()
{
    return false;
}

void PlantView::die()
{
    setActive(false);
}
