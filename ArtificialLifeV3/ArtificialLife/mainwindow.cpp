#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "animalview.h"
#include "engine.h"
#include "onmapobject.h"
#include "plant.h"
#include "objectsgenerator.h"

MainWindow::MainWindow(QWidget *parent) : QMainWindow(parent),ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    Engine::getInstance();
    map = new Map(this);
    //map->instantiateCarnivore(nullptr, Vector2(100, 550));
    ObjectsGenerator generator(map);
    generator.generate(10, 10, 10);
    //map->instanitate(nullptr, Vector2(500, 500));
    //animal = new AnimalView(this, Vector2(100, 550), 1);
    //animal2 = new AnimalView(this, Vector2(500, 500), 2);
    //animal->setAnimation(Vector2(100, 550), Vector2(700, 550));
    //plant = new PlantView();
}

MainWindow::~MainWindow()
{
    delete ui;
    //delete animal;
   // delete animal2;
}


QPushButton *MainWindow::getPlusButton() const
{
    return ui->time_plus;
}
QPushButton *MainWindow::getMinusButton() const
{
    return ui->time_minus;
}



void MainWindow::updateMenu(float speed,float perception ,float endurance,float strength,
                            float regeneration ,float energy,float health,int age, Sex sex)
{
    if (sex == Sex::male)
        ui->animal_label->setText(QStringLiteral("Sex:\t%1\n").arg("male"));
    else
        ui->animal_label->setText(QStringLiteral("Sex:\t%1\n").arg("female"));

    ui->age_bar->setValue(age);
    ui->health_bar->setValue(static_cast<int>(health));
    ui->energy_bar->setValue(static_cast<int>(energy));

    auto text = QStringLiteral("Speed:\t\t%1\n").arg(static_cast<double>(speed));
    text.append(QStringLiteral("Perception:\t%1\n").arg(static_cast<double>(perception)));
    text.append(QStringLiteral("Endurance:\t%1\n").arg(static_cast<double>(endurance)));
    text.append(QStringLiteral("Strength:\t\t%1\n").arg(static_cast<double>(strength)));
    text.append(QStringLiteral("Regeneration:\t%1\n").arg(static_cast<double>(regeneration)));
    ui->attributes_label->setText(text);
}

void MainWindow::setDate(int day, int hour, int minutes) const
{
    auto text = QStringLiteral("Date: %1 day").arg(day);
    if (hour < 10)
         text.append(QStringLiteral(", 0%1:").arg(hour));
    else
        text.append(QStringLiteral(", %1:").arg(hour));
    if (minutes < 10)
        text.append(QStringLiteral("0%1").arg(minutes));
    else
        text.append(QStringLiteral("%1").arg(minutes));

    ui->date_label->setText(text);
}

void MainWindow::setTime(float time_speed) const
{
    ui->time_label->setText(QStringLiteral("Time speed: %1").arg(time_speed));
}
