#ifndef CARNIVOREVIEW_H
#define CARNIVOREVIEW_H

#include "animalview.h"

class CarnivoreView : public AnimalView
{
public:
    CarnivoreView(Map* map, Vector2 position);

private:
    void initializeStates();
    State* createSearchState();
    State* createFollowState();
    State* createEatState();
    State* createFollowPartnerState();
    State* createRestState();

private slots:
    void searchingForFood();
    void followingFood();
    void eating();
    void runningAway();
    void searchingForPartner();
    void followingPartner();
    void reproducing();
    void enterSearch();
    void resting();
    void breeding();
    virtual void breed(AnimalViewModel* animal);
};

#endif // CARNIVOREVIEW_H
