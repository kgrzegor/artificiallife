#include "health.h"

Health::Health():Health(100.0f){}

Health::Health(float maxHealth):maxHealth_(maxHealth),currentHealth_(maxHealth_){}

bool Health::isDead() const
{
    return currentHealth_<=0;
}

void Health::hurt(float damage)
{
    if(damage <= 0) return; //exception there

    currentHealth_ -= damage;

    if(currentHealth_ < 0)
        currentHealth_ = 0;
}

void Health::heal(float healthAmount)
{
    if(healthAmount <= 0) return; //exception there

    currentHealth_ += healthAmount;

    if(currentHealth_ > maxHealth_)
        currentHealth_ = maxHealth_;
}

float Health::getMaxHealth() const
{
    return maxHealth_;
}

float Health::getHealth() const
{
    return currentHealth_;
}
