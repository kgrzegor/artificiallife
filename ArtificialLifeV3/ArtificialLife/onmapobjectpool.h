#ifndef ONMAPOBJECTPOOL_H
#define ONMAPOBJECTPOOL_H

#include <vector>
#include "plantview.h"
#include "carnivoreview.h"
#include "herbivoreview.h"

class OnMapObjectPool
{
public:
    static OnMapObjectPool* getInstnace();
    PlantView* getPlant(Map* map);
    CarnivoreView* getCarnivore(Map* map);
    HerbivoreView* getHerbivore(Map* map);
private:
    static OnMapObjectPool *instance;
    std::vector<PlantView*> plants;
    std::vector<CarnivoreView*> carnivores;
    std::vector<HerbivoreView*> herbivores;
    OnMapObjectPool();
};

#endif // ONMAPOBJECTPOOL_H
