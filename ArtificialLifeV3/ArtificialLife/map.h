#ifndef MAP_H
#define MAP_H

#include <QObject>
#include <vector>
#include "animalviewmodel.h"
#include "vector2.h"
#include "plant.h"


class OnMapObject;

class Map : QObject
{
    Q_OBJECT
public:
    Map(QWidget* widget);
    //void instantiate(OnMapObject object, float x, float y);
    void instantiatePlant(Plant* object, Vector2 position);
    void instantiateHerbivore(AnimalViewModel* object, Vector2 position);
    void instantiateCarnivore(AnimalViewModel* object, Vector2 position);
    QWidget* getWidget();
    std::vector<OnMapObject*>* getMapObjects();
    void returnObjectsIntoBorders();
    ~Map();

private:
    std::vector<OnMapObject*> objects;
    Vector2 leftUpBorder;
    Vector2 rightDownBorder;
    QWidget* widget;

private slots:
    void update();
};

#endif // MAP_H
