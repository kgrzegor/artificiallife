#ifndef PLANTVIEW_H
#define PLANTVIEW_H

#include <QObject>
#include "onmapobject.h"

class PlantView : public OnMapObject
{
private:
    virtual void update();
public:
    PlantView(Map* map, Vector2 positon);
    virtual Sex getSex();
    virtual bool isBreedCapable();
private slots:
    void die();
};

#endif // PLANTVIEW_H
