#ifndef HERBIVOREVIEW_H
#define HERBIVOREVIEW_H

#include "animalview.h"

class HerbivoreView : public AnimalView
{
public:
    HerbivoreView(Map* map, Vector2 position, int debug = 0);
private:
    void initializeStates();
    State* createSearchState();
    State* createFollowState();
    State* createEatState();
    State* createRunState();
    State* createBreedState();
    State* createRestState();

private slots:
    void searchingForFood();
    void followingFood();
    void eating();
    void runningAway();
    void breed(AnimalViewModel* animal);
    void enterSearch();
    void resting();
    void breeding();
    void followingPartner();
};

#endif // HERBIVOREVIEW_H
