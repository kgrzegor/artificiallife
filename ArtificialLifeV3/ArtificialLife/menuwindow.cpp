#include "menuwindow.h"
#include "ui_menuwindow.h"

MenuWindow::MenuWindow(QWidget *parent) : QMainWindow(parent),
    ui(new Ui::MenuWindow)
{
    ui->setupUi(this);
}

MenuWindow::~MenuWindow()
{
    delete ui;
}

void MenuWindow::on_start_button_clicked()
{
    window = new MainWindow(this);
    world_time_ = new WorldTime(window,this);
    window->show();
}

void MenuWindow::on_exit_button_clicked()
{
    QCoreApplication::quit();
}
