#ifndef STATE_H
#define STATE_H

#include <QObject>

class State : public QObject
{
    Q_OBJECT
public:
    explicit State(QObject *parent = nullptr);
    void enter();
    void update();
    void exit();
    void setActive(bool active);

private:
    bool isActive;

signals:
    void onEnter();
    void onUpdate();
    void onExit();

public slots:
    void onEngineUpdate();
};

#endif // STATE_H
