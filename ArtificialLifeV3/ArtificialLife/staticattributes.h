#ifndef STATICATTRIBUTES_H
#define STATICATTRIBUTES_H
#include "sex.h"
#include "random.h"
struct StaticAttributes //might be change to class
{
    Sex sex_;
    const float speed_;
    const float perception_;
    const float endurance_;
    const float strength_;
    const float regeneration_;

    StaticAttributes(float speed, float perception, float endurance, float strength, float regeneration);
};

#endif // STATICATTRIBUTES_H
