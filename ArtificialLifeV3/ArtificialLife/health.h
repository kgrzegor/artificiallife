#ifndef HEALTH_H
#define HEALTH_H


class Health
{
private:
    const float maxHealth_;
    float currentHealth_;
public:
    Health();
    Health(float maxHealth);
    bool isDead() const;
    void hurt(float damage);
    void heal(float healthAmount);
    float getHealth() const;
    float getMaxHealth() const;
};

#endif // HEALTH_H
