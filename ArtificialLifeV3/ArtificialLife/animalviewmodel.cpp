#include "animalviewmodel.h"
#include <QDebug>


float AnimalViewModel::createAttribute(float attribute1, float attribute2)
{
    float mutation_rate = Random::getMutationRate();
    return (attribute1 * attribute2)/2.0f * mutation_rate;
}

AnimalViewModel::AnimalViewModel(float energy, float perception, float endurance, float strength, QObject *parent)
    : QObject(parent),energy_(energy),attributes_(normal_endurance/endurance * 0.001f, perception, endurance, strength, normal_energy/energy), age_(0)
{} // those stats in constructor have to be changed

AnimalViewModel::AnimalViewModel(const AnimalViewModel &other):QObject(other.parent()),
    health_(),energy_(other.getMaxEnergy()),attributes_(other.getAttributes()), age_(0)
{}

int AnimalViewModel::getAge() const
{
    return age_;
}



AnimalViewModel* AnimalViewModel::breed(AnimalViewModel &parent1, AnimalViewModel &parent2)
{
    auto stats1 = parent1.getAttributes();
    auto stats2 = parent2.getAttributes();
    if (stats1.sex_ == stats2.sex_)
        throw std::runtime_error("Same sex breeding"); //should be change to better exception

    float energy = createAttribute(parent1.getMaxEnergy(),parent2.getMaxEnergy());

    //float speed = createAttribute(stats1.speed_, stats2.speed_);
    float perception = createAttribute(stats1.perception_, stats2.perception_);
    float endurance = createAttribute(stats1.endurance_, stats2.endurance_);
    float strength = createAttribute(stats1.strength_, stats2.strength_);
    //float regeneration = createAttribute(stats1.regeneration_, stats2.regeneration_);
    AnimalViewModel* newAnimal = new AnimalViewModel(energy,perception,endurance,strength);
    parent1.hasBred=true;
    parent2.hasBred=true;
    parent1.notifyView(newAnimal);
    return newAnimal;
}

AnimalViewModel* AnimalViewModel::random()
{
    float energy = Random::getRandomFloat(70,130);
    float perception = Random::getRandomFloat(40,100);
    float endurance = Random::getRandomFloat(5,15);
    float strength = Random::getRandomFloat(5,15);
    return new AnimalViewModel(energy,perception,endurance,strength);
}

void AnimalViewModel::notifyView(AnimalViewModel* newAnimal)
{
    emit breed(newAnimal);
}


void AnimalViewModel::hurt(float damage)
{
    health_.hurt(damage/attributes_.endurance_); //should be balanced
}

void AnimalViewModel::heal(float healthAmount)
{
    health_.heal(healthAmount * attributes_.regeneration_);//should be balanced
}

float AnimalViewModel::getHealth() const
{
    return health_.getHealth();
}
float AnimalViewModel::getMaxHealth() const
{
    return health_.getMaxHealth();
}

bool AnimalViewModel::isBreedingAge() const
{
    return (!hasBred && age_ > minumum_age_) ;
}

void AnimalViewModel::fatigue(float usedEnergy)
{
    energy_.fatigue(usedEnergy * 0.1f * attributes_.speed_); //should be balanced
    if(energy_.getEnergy() <= 0)
        emit exhausted();
}

void AnimalViewModel::rest(float energyAmount)
{
    energy_.rest(energyAmount * attributes_.regeneration_); //should be balanced
    if(energy_.getEnergy() >= energy_.getMaxEnergy())
        emit rested();
}

float AnimalViewModel::getEnergy() const
{
    return energy_.getEnergy();
}

float AnimalViewModel::getMaxEnergy() const
{
    return energy_.getMaxEnergy();
}

const StaticAttributes& AnimalViewModel::getAttributes() const
{
    return attributes_;
}

void AnimalViewModel::increaseAge()
{
    ++age_;
    if (age_ > minumum_age_ && Random::randomChoice((age_-minumum_age_)/300.0)) //max age == minumum_age_ + 99 but thats is VERY unlikely to achieve max_age
    {
        qDebug()<< "Died age: " << age_;
        emit die();
    }
}
