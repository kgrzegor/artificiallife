#ifndef PLANT_H
#define PLANT_H
#include "ilivingobject.h"
#include "health.h"

class Plant : public ILivingObject
{
private:
    Health health;
public:
    Plant(float maxHealth);

    // ILivingObject interface
public:
    void hurt(float damage);
    void heal(float healthAmount);
    float getHealth() const;
};

#endif // PLANT_H
