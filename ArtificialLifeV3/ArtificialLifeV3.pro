TEMPLATE = subdirs
QT += testlib
SUBDIRS += \
    AL \
    healthTests

AL.subdir = ./ArtificialLife
healthTests.subdir = ./HealthTests
